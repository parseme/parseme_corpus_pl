cat $1 | grep -v "# metadata" | awk 'BEGIN{FS=OFS="\t"}{if (NF == 11 && $0 !~ "^#"){ print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,"_"; } else { print $0; }}'
